from code.classification import __soutenance_classification
from code.regression import __soutenance_regression
from code.soutenance_weather_ml import __soutenance_ml_weather

if __name__ == '__main__':
    __soutenance_regression()
    __soutenance_classification()
    __soutenance_ml_weather()
