########################################################################################################################
# IMPORT
########################################################################################################################
# STD
import os

# CUSTOM
from code import project_root


# DATA
import pandas as pd
import numpy as np

# ML MODELS
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

# ML METHODS
from sklearn.model_selection import train_test_split
from sklearn.metrics import ConfusionMatrixDisplay, classification_report
from sklearn.model_selection import cross_val_predict, cross_val_score
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score, roc_curve

# VISU
import matplotlib.pyplot as plt
import seaborn as sns


########################################################################################################################
# VARIABLES
########################################################################################################################
ML_DIR = os.path.join(project_root(), 'data')
FEATURES = ['temp', 'pres', 'wind']
TARGET = 'POURCENTAGE_PARTICIPATION'


########################################################################################################################
# METHODS
########################################################################################################################
def prepare_data(_df, _features, _target, _size=.2):
    # x = _df.drop(_features, axis=1)
    x = _df[_features]
    y = _df[_target]
    print(f'Dataset size : {len(y)}')
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=_size, random_state=42)
    return x_train, x_test, y_train, y_test


def _standardize_min_max(_df, _col):
    min_value = _df[_col].min()
    max_value = _df[_col].max()

    scaled_col = (_df[_col] - min_value) / (max_value - min_value)

    _df[_col] = scaled_col
    return _df


def _standardize_z_score(_df, _col):
    mean_value = _df[_col].mean()
    std_value = _df[_col].std()

    standardized_col = (_df[_col] - mean_value) / std_value

    _df[_col] = standardized_col
    return _df


def _col_stats(_df, _col):
    max_value = _df[_col].max()
    min_value = _df[_col].min()
    avg_value = _df[_col].mean()
    std_value = _df[_col].std()
    median_value = _df[_col].median()
    print(f'Col : {_col}')
    print(f'Maximum value: {max_value}')
    print(f'Minimum value: {min_value}')
    print(f'Average value: {avg_value}')
    print(f'Standard deviation: {std_value}')
    print(f'Median : {median_value}')


def __soutenance_ml_weather():
    d = pd.read_csv(os.path.join(ML_DIR, 'weather_for_ml_2.csv'))
    d = d.sample(n=500)
    d = d[['temp', 'pres', 'wind', 'POURCENTAGE_PARTICIPATION']]

    d = _standardize_z_score(d, 'wind')
    d = _standardize_z_score(d, 'temp')
    d = _standardize_z_score(d, 'pres')

    col = 'POURCENTAGE_PARTICIPATION'
    print(d.columns)
    median_value = d[col].median()

    d['POURCENTAGE_PARTICIPATION'] = d['POURCENTAGE_PARTICIPATION'] > median_value + 1

    def svm(_data, _kernel, _features, _target, ):
        print(f'SVM : {_kernel} | {_features} > {_target}')
        x_train, x_test, y_train, y_test = prepare_data(_df=_data, _features=_features, _target=_target)

        svm_model = SVC(kernel=_kernel, C=1.0, random_state=42)
        svm_model.fit(x_train, y_train)

        y_pred = svm_model.predict(x_test)

        accuracy = accuracy_score(y_test, y_pred)
        conf_matrix = confusion_matrix(y_test, y_pred)

        disp = ConfusionMatrixDisplay(confusion_matrix=conf_matrix)
        disp.plot()
        plt.show()

        # classification_rep = classification_report(y_test, y_pred)
        classification_rep = classification_report(y_test, y_pred, zero_division=1)  #

        print(f'Accuracy: {accuracy}')
        print(f'Confusion Matrix:\n{conf_matrix}')
        print(f'Classification Report:\n{classification_rep}')

        new_data = np.random.rand(len(x_train.columns)).reshape(1, -1)

        prediction = svm_model.predict(new_data)
        print(f'New Value : {new_data} | Predicted : {prediction}')

        decision_scores = svm_model.decision_function(x_test)
        # print(f'Decision Scores for the test set:\n{decision_scores}')

        if _kernel == 'linear':
            feature_importance = svm_model.coef_[0]
            feature_importance_df = pd.DataFrame({'Feature': x_train.columns, 'Importance': feature_importance})
            print(feature_importance_df)

    #
    # svm(d, _kernel='sigmoid', _features=['temp', 'pres', 'wind'], _target='POURCENTAGE_PARTICIPATION')
    # svm(d, _kernel='linear', _features=['temp', 'pres', 'wind'], _target='POURCENTAGE_PARTICIPATION')
    # svm(d, _kernel='linear', _features=['temp', 'wind'], _target='POURCENTAGE_PARTICIPATION')
    #

    logistic_reg(d, _features=['temp', 'pres', 'wind'], _target='POURCENTAGE_PARTICIPATION')

    ml_process_roc(d[['temp', 'pres', 'wind']], d['POURCENTAGE_PARTICIPATION'])


def logistic_reg(data, _features, _target):
    print('______________ LOGISTIC\n')
    # print_dataset(data)
    X = data[_features]
    y = data[_target]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    model = LogisticRegression()
    model.fit(X_train, y_train)

    y_pred = model.predict(X_test)

    accuracy = accuracy_score(y_test, y_pred)
    print(f'Accuracy: {accuracy}')

    print(classification_report(y_test, y_pred))

    new_data = np.random.rand(len(_features)).reshape(1, -1)
    new_predictions = model.predict(new_data)

    print(new_predictions)

    probabilities = model.predict_proba(new_data)

    print(probabilities)
    print('______________\n')


def train_models_and_evaluate(X_train, y_train, models):
    results = {}
    roc_curves = {}

    for model_name, model in models.items():
        print(f'train_models_and_evaluate : {model_name}')
        # print(f'====== {model_name} -- {X_train} -- {y_train}')
        model.fit(X_train, y_train)

        cv_scores = cross_val_score(model, X_train, y_train, cv=5)

        y_pred_cv = cross_val_predict(model, X_train, y_train, cv=5)

        accuracy = accuracy_score(y_train, y_pred_cv)
        precision = precision_score(y_train, y_pred_cv)
        recall = recall_score(y_train, y_pred_cv)
        f1 = f1_score(y_train, y_pred_cv)

        cm = confusion_matrix(y_train, y_pred_cv)

        plt.figure(figsize=(8, 6))
        sns.heatmap(cm, annot=True, cmap='Blues', fmt='g')
        plt.title(f'Confusion Matrix - {model_name}')
        plt.xlabel('Predicted')
        plt.ylabel('Actual')
        plt.show()

        results[model_name] = {
            'Accuracy': accuracy,
            'Precision': precision,
            'Recall': recall,
            'F1 Score': f1,
            'Cross-Validation Scores': cv_scores
        }

        validation = {
            'Accuracy': accuracy,
            'Precision': precision,
            'Recall': recall,
            'F1 Score': f1,
            'Dataset Len': len(X_train),
        }
        # import pickle
        # import json
        # filename = 'knn5_weather.sav'
        # pickle.dump(model, open(filename, 'wb'))
        # with open('../knn5_weather.json', 'w') as f:
        #     json.dump(validation, f)

        if hasattr(model, 'predict_proba'):
            y_scores = cross_val_predict(model, X_train, y_train, cv=5, method='predict_proba')[:, 1]
            fpr, tpr, thresholds = roc_curve(y_train, y_scores)
            roc_curves[model_name] = {
                'fpr': fpr,
                'tpr': tpr
            }

    return results, roc_curves


def ml_process_roc(_x, _y):
    X_train = _x
    y_train = _y

    models = {
        'KNN (k=2)': KNeighborsClassifier(n_neighbors=2),
        'KNN (k=3)': KNeighborsClassifier(n_neighbors=3),
        'KNN (k=5)': KNeighborsClassifier(n_neighbors=5),
        'SVM (sigmoid)': SVC(kernel='sigmoid', probability=True),
        'SVM (linear)': SVC(kernel='linear')
    }

    results, roc_curves = train_models_and_evaluate(X_train, y_train, models)

    for model_name, result in results.items():
        print(f"Model: {model_name}")
        print(pd.Series(result))
        print('\n')

    # ROC
    plt.figure(figsize=(8, 6))
    for model_name, roc_curve_data in roc_curves.items():
        plt.plot(roc_curve_data['fpr'], roc_curve_data['tpr'], label=model_name)
    plt.text(0.2, 0, f'Features : Vent, Pression atmosphérique, Température\nTarget : Pourcentage de participation',
             fontsize=8)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curves')
    plt.legend()
    plt.show()
