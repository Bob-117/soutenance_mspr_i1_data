import os


def project_root(start_dir=None):
    known_dir_name = 'soutenance_mspr_i1_data'

    if start_dir is None:
        start_dir = os.getcwd()

    current_dir = os.path.abspath(start_dir)
    while True:

        # Filesystem root check
        if current_dir == os.path.dirname(current_dir):
            return None

        # Specific files or directories
        if (
                current_dir.endswith(known_dir_name)
                or any(dirname in os.listdir(current_dir) for dirname in ['venv', 'env'])
                or 'src' in os.listdir(current_dir)
                or any(dirname in os.listdir(current_dir) for dirname in ['.git'])
        ):
            return current_dir

        current_dir = os.path.dirname(current_dir)


if __name__ == '__main__':
    __project_root = project_root()
    print("Python project root:", __project_root) if __project_root else print("No Python project root found.")

