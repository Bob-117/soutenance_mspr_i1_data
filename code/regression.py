import os

import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score
import joblib

from code import project_root


def __soutenance_regression():
    # LOAD & TRANSFORM
    _d = pd.read_csv(os.path.join(project_root(), 'data', 'regression.csv'))
    _d = _d.fillna(_d.mean(numeric_only=True))
    _d['bord'] = _d['bord'].astype('category')
    _d.dropna(subset=['bord'], inplace=True)

    bord_mapping = {'droite': 0, 'centre': 1, 'gauche': 2}
    reverse_bord_mapping = {v: k for k, v in bord_mapping.items()}
    _d['bord'] = _d['bord'].map(bord_mapping)

    # Train
    X = _d.drop('% Exprimés', axis=1)
    y = _d['% Exprimés']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    lr_model = LinearRegression()
    lr_model.fit(X_train, y_train)

    rf_model = RandomForestRegressor(n_estimators=100)
    rf_model.fit(X_train, y_train)

    # Validate
    def print_metrics(model, X_test, y_test):
        y_pred = model.predict(X_test)
        mse = mean_squared_error(y_test, y_pred)
        r2 = r2_score(y_test, y_pred)
        if mse < 10 or r2 > 0.2:
            print('STOP')
            print(f'Mean Squared Error: {mse}')
            print(f'R-squared: {r2}')
            joblib.dump(model, 'huho.pkl')
        print(f'Mean Squared Error: {mse}')
        print(f'R-squared: {r2}')

    print("Linear Regression Metrics:")
    print_metrics(lr_model, X_test, y_test)
    print("\nRandom Forest Regression Metrics:")
    print_metrics(rf_model, X_test, y_test)

    # PLot
    def plot_predictions(model, X_test, y_test):
        y_pred = model.predict(X_test)
        plt.scatter(y_test, y_pred)
        plt.xlabel('Actual Values')
        plt.ylabel('Predicted Values')
        plt.title(f'Actual vs Predicted Values : {model}')
        plt.show()

    print("\nLinear Regression Plot:")
    plot_predictions(lr_model, X_test, y_test)
    print("\nRandom Forest Regression Plot:")
    plot_predictions(rf_model, X_test, y_test)

    # # Save
    # joblib.dump(lr_model, os.path.join(project_root(), 'models', 'linear_regression_model.pkl'))
    # joblib.dump(rf_model, os.path.join(project_root(), 'models', 'random_forest_regression_model.pkl'))
    #
    # # Reload
    # lr_model_loaded = joblib.load(os.path.join(project_root(), 'models', 'linear_regression_model.pkl'))
    # rf_model_loaded = joblib.load(os.path.join(project_root(), 'models', 'random_forest_regression_model.pkl'))

    # Usage
    # def predict(model, X):
    #     return model.predict(X)
    #
    # for _bord in ['gauche', 'droite', 'centre']:
    #     # Features
    #     nb_lit_value = 2450
    #     chomage_value = 7.2
    #     _bord_numerical = bord_mapping[_bord]
    #     _bord_categorical = reverse_bord_mapping[_bord_numerical]
    #
    #     X_new = pd.DataFrame({'bord': [_bord_numerical], 'nb_lit': [nb_lit_value], 'chomage': [chomage_value]})
    #
    #     print(f"Prediction (bord : {_bord_categorical}, nb lit : {nb_lit_value}, chomage : {chomage_value}")
    #
    #     print("Prediction with Reloaded Linear Regression Model:")
    #     print(predict(lr_model_loaded, X_new))
    #
    #     print("Prediction with Reloaded Random Forest Regression Model:")
    #     print(predict(rf_model_loaded, X_new))


if __name__ == '__main__':
    __soutenance_regression()
