import json
import os
import tkinter as tk
from tkinter import ttk
import pickle

import joblib
from code import project_root


def load_model():
    model_weather = pickle.load(open(os.path.join(project_root(), 'models', 'knn5_weather.sav'), 'rb'))
    with open(os.path.join(project_root(), 'models', 'knn5_weather.json')) as data_file:
        model_weather_info = json.load(data_file)

    model_political = pickle.load(open(os.path.join(project_root(), 'models', 'RandomForestClassifier.sav'), 'rb'))
    with open(os.path.join(project_root(), 'models', 'RandomForestClassifier.json')) as data_file:
        model_political_info = json.load(data_file)

    return {
        'kn5': {
            'model': model_weather,
            'info': model_weather_info
        },
        'forest': {
            'model': model_political,
            'info': model_political_info
        },
        'other': {
            'model': 'other',
            'info': {
                "Accuracy": 0,
                "Precision": 0,
                "Recall": 0,
                "F1 Score": 0,
                "Dataset Len": 10,
                "FEATURES": ["a", "b", "c", "d", "e"],
                "TARGET": "huho"}
        },
    }


class PoliticalApp:
    def __init__(self):
        self.width = 400
        self.height = 600
        self.__title = "Political App"
        self.root = tk.Tk()
        self.root.title(self.__title)
        self.root.geometry(f"{self.width}x{self.height}")

        self.models = load_model()
        self.selected_model = tk.StringVar()
        self.input_vars = []
        self.info_box = None
        self.input_frame = None
        self.result_box = None
        self.predict_button = None

        self.create_ui()

    def create_ui(self):
        model_dropdown = ttk.Combobox(self.root, textvariable=self.selected_model)
        model_dropdown['values'] = list(self.models.keys())
        model_dropdown.grid(row=0, column=0, padx=10, pady=10)
        model_dropdown.bind("<<ComboboxSelected>>", self.show_model_info)

        self.info_box = tk.Text(self.root, width=50, height=10)
        self.info_box.grid(row=1, column=0, padx=10, pady=10)
        self.info_box.grid_remove()

        self.input_frame = ttk.Frame(self.root)
        self.input_frame.grid(row=2, column=0, padx=10, pady=10)
        self.input_frame.grid_remove()

        self.predict_button = ttk.Button(self.root, text="Predict", command=self.predict)
        self.predict_button.grid(row=3, column=0, padx=10, pady=10)
        self.predict_button.grid_remove()

        self.result_box = tk.Text(self.root, width=50, height=10)
        self.result_box.grid(row=4, column=0, padx=10, pady=10)
        self.result_box.grid_remove()

    def show_model_info(self, event=None):
        selected = self.selected_model.get()
        model_info = self.models[selected]['info']

        self.info_box.delete('1.0', tk.END)
        self.result_box.delete('1.0', tk.END)
        self.info_box.insert(tk.END, f"Accuracy: {model_info['Accuracy']}\n")
        self.info_box.insert(tk.END, f"Precision: {model_info['Precision']}\n")
        self.info_box.insert(tk.END, f"Recall: {model_info['Recall']}\n")
        self.info_box.insert(tk.END, f"F1 Score: {model_info['F1 Score']}\n")
        self.info_box.insert(tk.END, f"Dataset Len: {model_info['Dataset Len']}\n")
        self.info_box.insert(tk.END, f"Features: {', '.join(model_info['FEATURES'])}\n")
        self.info_box.insert(tk.END, f"Target: {model_info['TARGET']}")

        self.info_box.grid()
        self.input_frame.grid()
        self.result_box.grid()
        self.predict_button.grid()

        for widget in self.input_frame.winfo_children():
            widget.destroy()
        self.input_vars = []
        for feature in model_info['FEATURES']:
            var = tk.DoubleVar()
            ttk.Label(self.input_frame, text=feature).grid(row=len(self.input_vars), column=0, padx=5, pady=5)
            ttk.Entry(self.input_frame, textvariable=var).grid(row=len(self.input_vars), column=1, padx=5, pady=5)
            self.input_vars.append(var)

    def predict(self):
        selected = self.selected_model.get()
        # model_info = self.models[selected]['info']
        model = self.models[selected]['model']

        input_values = [var.get() for var in self.input_vars]
        prediction = model.predict([input_values])[0]

        self.result_box.delete('1.0', tk.END)
        self.result_box.insert(tk.END, f"Prediction: {prediction}")

    def start(self):
        self.root.mainloop()


if __name__ == '__main__':
    PoliticalApp().start()
