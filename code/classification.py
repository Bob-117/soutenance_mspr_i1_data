import os
import pickle

import joblib
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, roc_curve, auc
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from imblearn.over_sampling import SMOTE
import seaborn as sns

from code import project_root


def __logistic_reg(X_train, X_test, y_train, y_test):
    print('LOGISTIC')
    model_lr = LogisticRegression(multi_class='multinomial', solver='lbfgs')
    model_lr.fit(X_train, y_train)
    y_pred_lr = model_lr.predict(X_test)
    print('Régression logistique multinomiale :')
    print('Accuracy :', accuracy_score(y_test, y_pred_lr))
    print('F1-score :', f1_score(y_test, y_pred_lr, average='macro'))
    print('Precision :', precision_score(y_test, y_pred_lr, average='macro'))
    print('Recall :', recall_score(y_test, y_pred_lr, average='macro'))

    cm_lr = confusion_matrix(y_test, y_pred_lr)
    plt.figure(figsize=(8, 6))
    sns.heatmap(cm_lr, annot=True, cmap='Blues', fmt='g')
    plt.title('Matrice de confusion - Régression logistique multinomiale')
    plt.xlabel('Prédictions')
    plt.ylabel('Valeurs réelles')
    plt.show()

    # print('roc')
    # fpr_lr, tpr_lr, _ = roc_curve(y_test, model_lr.predict_proba(X_test)[:, 1])
    # roc_auc_lr = auc(fpr_lr, tpr_lr)
    # plt.figure(figsize=(8, 6))
    # plt.plot(fpr_lr, tpr_lr, color='darkorange', lw=2, label='ROC curve (AUC = %0.2f)' % roc_auc_lr)
    # plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    # plt.xlim([0.0, 1.0])
    # plt.ylim([0.0, 1.05])
    # plt.xlabel('Taux de faux positifs')
    # plt.ylabel('Taux de vrais positifs')
    # plt.title('Courbe ROC - Régression logistique multinomiale')
    # plt.legend(loc="lower right")
    # plt.show()


def __nn(X_train, X_test, y_train, y_test):
    print('nn')
    model_ann = MLPClassifier(hidden_layer_sizes=(100,), max_iter=500, random_state=42)
    model_ann.fit(X_train, y_train)
    y_pred_ann = model_ann.predict(X_test)
    print('\nRéseaux de neurones artificiels :')
    print('Accuracy :', accuracy_score(y_test, y_pred_ann))
    print('F1-score :', f1_score(y_test, y_pred_ann, average='macro'))
    print('Precision :', precision_score(y_test, y_pred_ann, average='macro'))
    print('Recall :', recall_score(y_test, y_pred_ann, average='macro'))
    #

    print('conf')
    cm_ann = confusion_matrix(y_test, y_pred_ann)
    plt.figure(figsize=(8, 6))
    sns.heatmap(cm_ann, annot=True, cmap='Blues', fmt='g')
    plt.title('Matrice de confusion - Réseaux de neurones artificiels')
    plt.xlabel('Prédictions')
    plt.ylabel('Valeurs réelles')
    plt.show()


def __soutenance_classification():
    _d = pd.read_csv(os.path.join(project_root(), 'data', 'winners.csv'))
    _d = _d.fillna(_d.mean(numeric_only=True))
    X = _d[['chomage', 'nb_lit']]
    y = _d['bord']

    smote = SMOTE(k_neighbors=1)
    X, y = smote.fit_resample(X, y)

    print(len(X))
    print(len(y))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

    # __logistic_reg(X_train, X_test, y_train, y_test)
    ######################
    model_rf = RandomForestClassifier(n_estimators=100, random_state=42)
    model_rf.fit(X_train, y_train)
    print(X_test)
    y_pred_rf = model_rf.predict(X_test)
    print('\nForêts aléatoires :')
    print('Accuracy :', accuracy_score(y_test, y_pred_rf))
    print('F1-score :', f1_score(y_test, y_pred_rf, average='macro'))
    print('Precision :', precision_score(y_test, y_pred_rf, average='macro'))
    print('Recall :', recall_score(y_test, y_pred_rf, average='macro'))

    cm_rf = confusion_matrix(y_test, y_pred_rf)
    plt.figure(figsize=(8, 6))
    sns.heatmap(cm_rf, annot=True, cmap='Blues', fmt='g')
    plt.title('Matrice de confusion - Forêts aléatoires')
    plt.xlabel('Prédictions')
    plt.ylabel('Valeurs réelles')
    plt.show()

    # pickle.dump(model_rf, open('RandomForestClassifier.sav', 'wb'))

    # pred = model_rf.predict(np.array([1, 5]).reshape(1, -1))
    # print(f'Prediction : {pred}')
    ######################
    # __nn(X_train, X_test, y_train, y_test)
