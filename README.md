# Political Machine Learning


```shell
git clone https://gitlab.com/Bob-117/soutenance_mspr_i1_data
cd soutenance_mspr_i1_data
python -m venv venv 
source venv/bin/activate
pip install -r requirements.txt

python main.py

# Some data and models are available in data/ and models/, make sure you import your own files for better experience.
```

## Data

- Elections : https://www.archives-resultats-elections.interieur.gouv.fr/ 
- Chômage : https://www.insee.fr/fr/statistiques/serie/001515900 
- Hôpitaux : https://www.insee.fr/fr/statistiques/serie/001722491 
- Météo : https://github.com/meteostat/meteostat-python 


## Weather

- Individu : Election
- Features : Vent, Pression Atmospherique, Température
- Target : Participation (> threshold)

![](asset/knn.png)

![](asset/roc.png)


Conclusion : SVM peu performants, la limite géographique a un impact sur la variété des données.


## Regression

- Individu : Candidat
- Features : Chomage, Lit d'hopitaux, Bord Politique
- Target : % Exprimé

![](asset/regression.png)

Conclusion : Les différentes features sont faiblement corrélées à la target.

## Classification

- Individu : Candidat
- Features : Chomage, Lit d'hopitaux
- Target : Bord Politique (du gagnant)

![](asset/classification.png)

Conclusion : Le choix de 3 départements (Ille-et-Vilaine 35, Alpes-Maritimes 06 & Moselle 57) avec des résultats (politiques) particulièrement orientés a une incidence sur l'entrainement et l'utilisation d'un modele. 
Il faut etre conscient qu'un modèle de classification entrainé sur un dataset présentant des déséquilibres dans les populations des différentes classes doit être utilisé dans un contexte proche de l'entrainement. 


## App

```shell
python code/app.py
```

![](asset/app.png)


